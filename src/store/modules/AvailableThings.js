import AvailableThings from "@/store/data/AvailableThings";

export default {
    state () {
        return {
            selectedAvailableThing: false,
            availableThings: [],
        }
    },
    actions: {
        fetchAvailableThings(ctx) {
            ctx.commit('fetchAvailableThings', AvailableThings)
        },
        selectAvailableThing(ctx, thing) {
            ctx.commit('restoreSelectAvailableThing')
            ctx.commit('selectAvailableThing', thing)
            ctx.commit('removeSelectAvailableThing', thing)
        },
        restoreSelectAvailableThing(ctx) {
            ctx.commit('restoreSelectAvailableThing')
        },
    },
    mutations: {
        fetchAvailableThings (state, things) {
            state.availableThings = things;
        },
        selectAvailableThing (state, things) {
            state.selectedAvailableThing = things;
        },
        removeSelectAvailableThing (state, removeThing) {
            state.availableThings = state.availableThings.filter((thing) => {
                return removeThing.id !== thing.id;
            });
        },
        restoreSelectAvailableThing (state) {
            if (state.selectedAvailableThing) {
                state.availableThings.push(state.selectedAvailableThing);
                state.selectedAvailableThing = false;
            }
        }
    },
    getters: {
        getAvailableThings(state) {
            return state.availableThings;
        },
        getSelectedAvailableThing(state) {
            return state.selectedAvailableThing;
        },
    },
}
