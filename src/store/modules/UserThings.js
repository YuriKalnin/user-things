import UserThings from "@/store/data/UserThings";

export default {
    state () {
        return {
            userThings: [],
            selectedUserThings: [],
            maxSelectedUserThings: 6,
        }
    },
    actions: {
        fetchUserThings(ctx) {
            ctx.commit('updateUserThings', UserThings)
        },
        selectUserThing(ctx, thing) {
            ctx.commit('selectUserThing', thing)
            ctx.commit('removeUserThing', thing)
        },
        unSelectUserThing(ctx, thing) {
            ctx.commit('unSelectUserThing', thing)
            ctx.commit('removeSelectedUserThing', thing)
        },
    },
    mutations: {
        updateUserThings (state, userThings) {
            state.userThings = userThings;
        },
        removeUserThing (state, userThing) {
            state.userThings = state.userThings.filter((thing) => {
                return userThing.id !== thing.id;
            });
        },

        selectUserThing (state, userThing) {
            state.selectedUserThings.push(userThing);
        },
        unSelectUserThing (state, userThing) {
            state.userThings.push(userThing);
        },
        removeSelectedUserThing (state, userThing) {
            state.selectedUserThings = state.selectedUserThings.filter((thing) => {
                return userThing.id !== thing.id;
            });
        },
    },
    getters: {
        getUserThings(state) {
            return state.userThings;
        },
        getSelectedUserThings(state) {
            return state.selectedUserThings;
        },
        getCountSelectedUserThings(state) {
            return state.selectedUserThings.length;
        },
        getMaxSelectedUserThings(state) {
            return state.maxSelectedUserThings;
        },
    },
}
