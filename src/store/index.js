import { createStore } from 'vuex'
import UserThings from "@/store/modules/UserThings";
import AvailableThings from "@/store/modules/AvailableThings";

export default createStore({
    modules: {
        UserThings,
        AvailableThings,
    }
})

